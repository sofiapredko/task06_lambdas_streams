import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;
import static java.util.stream.Collectors.*;

public class StreamExample {
    public static void task3() {
        Random rand = new Random();
        List<Integer> myList = new ArrayList<>();
        int size = 20;
        for(int i = 0; i < size; i++) {
            int randNum= rand.nextInt(100);
            myList.add(randNum);
        }

        System.out.println("Original List: " + myList);

        Stream<Integer> newStream = myList.stream();

        newStream
                .filter(number -> number % 2 == 0)
                .peek(a -> out.print(a + " "))
                .collect(Collectors.toList());

        String stats = myList.stream()
                .mapToInt((a) -> a)
                .summaryStatistics()
                .toString();
        out.println();
        out.println("Summary statistics: " + stats);

        Optional<Integer> sumWithReduce = myList.stream()
                .reduce((a, b) -> a + b);

        out.println("Sum with reduce: " + sumWithReduce);

        int sum = myList.stream()
                .mapToInt((a) -> a)
                .sum();

        out.println("Sum with .sum: " + sum);

        OptionalDouble countBiggerNum = myList.stream()
                .mapToInt((a) -> a)
                .average();

        Stream<Integer> newStream1 = myList.stream();
        double average = countBiggerNum.getAsDouble();

        List<Integer> biggerThanAv = newStream1
                .filter(a -> a > average)
                .collect(Collectors.toList());

        out.println("There are " + biggerThanAv.size() + " numbers that are bigger than average: " + biggerThanAv);
    }

    public static void task4() {
        Scanner scan = new Scanner(System.in);
        out.println("Type your text: ");
        String userstr = scan.nextLine();
        List<String> myList = new ArrayList<String>(Arrays.asList(userstr.split(" ")));
        out.println(myList);

        List<String> listDistinct = myList.stream()
                .map(String::toLowerCase)
                //.map(name -> name.toUpperCase())
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        //  or   listDistinct.sort((p1, p2) -> p1.compareTo(p2));
        out.println("Unique words in text: " + listDistinct);

        out.println("Frequency of each word: ");
        Map<String, Long> mapc =
                myList.stream()
                        .map(String::toLowerCase)
                        .collect(groupingBy(Function.identity(), counting()));
        System.out.println(mapc);

        /*   another variant
        Map<String, Integer> counts = myList
                .parallelStream()
                .map(String::toLowerCase)
                .collect(Collectors.toConcurrentMap(
                        w -> w, w -> 1, Integer::sum));
        System.out.println(counts);

        /////    or

        List<String> listFreq = (List<String>) myList.stream()
                .map(String::toLowerCase)
                .collect(groupingBy(String::toString, counting()));
        */

    }


}
