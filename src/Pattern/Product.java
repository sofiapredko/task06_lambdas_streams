package Pattern;

//receiver
public class Product {
    private String name;

    public void buy(String name) {
        System.out.println("Pattern.Product[ Name: " + name + " ]");
    }
}
