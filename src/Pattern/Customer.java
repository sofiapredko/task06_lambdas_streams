package Pattern;

import java.util.ArrayList;
import java.util.List;

//Invoker
public class Customer {

    private List<Command> orderList = new ArrayList<Command>();

    public void takeOrder(Command order){
        orderList.add(order);
    }

    public void placeOrders(){

        for (Command order : orderList) {
            order.execute();
        }
        orderList.clear();
    }

    public static void myMethod(){
        System.out.println("Calling reference");
    }

}
