package Pattern;

import java.util.Scanner;

//Concrete Pattern.Command
public class ConcreteCommand implements Command {
    private Product product;
    static Scanner scanner = new Scanner(System.in);


    public ConcreteCommand(Product product){
        this.product = product;
        //this.name = name;
    }

    public String getName(){
        System.out.println("Put a name: ");
        String name = scanner.nextLine();
        return name;
    }

    @Override
    public void execute() {
        String name = getName();
        product.buy(name);
    }
}
