package Pattern;

@FunctionalInterface
public interface Command{
    public void execute();
}