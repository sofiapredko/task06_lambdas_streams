import Pattern.ConcreteCommand;
import Pattern.Customer;
import Pattern.Product;

import java.util.Random;
import java.util.Scanner;

public class TaskFunctions {
    public static void task1() {
        Random rand = new Random();
        int v1 = rand.nextInt(50) + 1;
        int v2 = rand.nextInt(50) + 1;
        int v3 = rand.nextInt(50) + 1;
        System.out.print("Three random numbers: ");
        MyFuncInterface lmprint = (a, b, c) -> {
            System.out.println(a + " " + b + " " + c);
            return 0;
        };
        lmprint.action(v1, v2, v3);
        MyFuncInterface lm1 = (a, b, c) -> Integer.max(a, Integer.max(b, c));
        System.out.println("Max number: " + lm1.action(v1, v2, v3));
        MyFuncInterface lm2 = (a, b, c) -> (a + b + c) / 3;
        System.out.println("avarage number: " + lm2.action(v1, v2, v3));
    }

    public static void task2() {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter what you want to do: ");
        System.out.println("simple - pattern calling command as object");
        System.out.println("lambda - pattern calling command as lambda function");
        System.out.println("anonclass - pattern calling command as an object of anonymous class");
        System.out.println("ref - pattern calling command as method reference");
        String nameofaction = scan.nextLine();

        switch(nameofaction){
            case("simple"):
                System.out.println("First variant: ");
                Pattern.Product p1 = new Pattern.Product();   //creating receiver
                Pattern.ConcreteCommand buyProduct1 = new Pattern.ConcreteCommand(p1); ///concrete command
                buyProduct1.execute();


                System.out.println("Second variant: ");
                Pattern.Product p2 = new Pattern.Product();
                Pattern.ConcreteCommand buyProduct2 = new Pattern.ConcreteCommand(p2);
                Pattern.Customer customer1 = new Pattern.Customer();
                customer1.takeOrder(buyProduct2);
                customer1.placeOrders();
                break;
            case("lambda"):
                Pattern.Product p3 = new Pattern.Product();
                Pattern.Customer customer3 = new Pattern.Customer();
                customer3.takeOrder(() -> {
                    p3.buy("all");  /// using lambda instead of Pattern.ConcreteCommand object
                });
                customer3.placeOrders();
                break;
            case("anonclass"):
                Pattern.Product p4 = new Pattern.Product();
                Pattern.ConcreteCommand buyProduct4 = new Pattern.ConcreteCommand(p4);
                Pattern.Customer customer4 = new Pattern.Customer() {
                    @Override
                    public void takeOrder(Pattern.Command order) {
                        super.takeOrder(order);
                        super.placeOrders();
                    }
                };
                customer4.takeOrder(buyProduct4);
                break;
            case("ref"):
                Product p5 = new Product();
                ConcreteCommand cmr = new ConcreteCommand(p5);
                Customer customer5 = new Customer();
                customer5.takeOrder(Customer::myMethod);
                customer5.takeOrder(cmr);
                customer5.placeOrders();
                cmr.execute();
            default:
                System.out.println();
                break;
        }
    }
}
