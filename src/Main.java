import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("1 - lambda functions");
        System.out.println("2 - Command Pattern");
        System.out.println("3 - Stream Operation with List");
        System.out.println("4 - Stream Operation with text and words");

        boolean perform = true;
        while (perform != false) {
            System.out.println();
            System.out.println("Enter number of task: ");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    TaskFunctions.task1();
                    break;
                case 2:
                    TaskFunctions.task2();
                    break;
                case 3:
                    StreamExample.task3();
                    break;
                case 4:
                    StreamExample.task4();
                    break;
                case 0:
                    System.out.println("Bye!");
                    System.exit(0);
            }
        }
    }
}
